<?php

//esta capacidad permite a los usuarios autorizados a leer
//context level puede cambiar, este es global
//en un curso tenes contextlevel = course
/* VALIDACION A MODO GLOBAL */
$capabilities = array(
    'local/codigoqr:viewcodigoqr' => array(
        'riskbitmask' => RISK_SPAM,
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        //DEBERIAN SER USUARIOS DE CURSOS.
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
);

/* 
    Inicialmente user no pertenece a ningun curso
    cuando se matriculan, ahi pertenecen a un curso ya sea como profesor, alumno todo dentro del contexto de curso
    entonces se utilizan otras capacidades.
*/
