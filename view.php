<?php

require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');

global $PAGE, $OUTPUT, $DB, $USER;

require_capability('local/codigoqr:viewcodigoqr', context_system::instance());

//traeme las reglas del external page 
admin_externalpage_setup('cod_qr');

require_once($CFG->libdir . '/tcpdf/tcpdf_barcodes_2d.php');
require_once($CFG->libdir . '/pdflib.php');

echo $OUTPUT->header();
echo "<h1>" . $USER->username . "</h1>";

const BARCODETYPE = 'QRCODE';

$qrcodeurl = new \moodle_url('/');
$qrcodeurl = $qrcodeurl->out(false);
$qrcodeurl .= "local/codigoqr/view.php";

$barcode = new \TCPDF2DBarcode($USER->email, BARCODETYPE);



echo $barcode->getBarcodeHTML(6, 6, 'black');
echo $OUTPUT->footer();
exit();