<?php

//siempre respetar formato sufijo
function local_codigoqr_extend_navigation(global_navigation $navigation) {   
    
    $url = new moodle_url('/local/codigoqr/view.php');
    
    // cambio hecho en master

    //creas nodo con icono url y eso
    $node = navigation_node::create(
        get_string('pluginname', 'local_codigoqr'),
        $url,
        navigation_node::TYPE_SETTING,
        null,
        null,
        new pix_icon('i/settings', '')
    );
    //quiero que lo muestre a la izquierda
    $node->showinflatnavigation = true;
    $index = $navigation->find("home",navigation_node::TYPE_SETTING);

    $node->classes = array('localcodigoqr');
    $node->key = 'localcodigoqr';


    //si tiene la capacidad, agregar la visualizacion del plugin
   
    /*
    if (isset($node) && has_capability('local/codigoqr:viewcodigoqr', context_system::instance())) {
        $index->add_node($node);
    }
    */

    if (isset($node) && has_capability('local/codigoqr:viewcodigoqr', context_system::instance())) {
        $index->add_node($node);
    }
    
}
