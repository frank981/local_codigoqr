<?php

//restringue acceso, hay veces que el link no lo vas a hacer desde settings.
$externalpage = new admin_externalpage('cod_qr', 'Enlace local codigoqr', $CFG->wwwroot.'/local/codigoqr/view.php', 'local/codigoqr:viewcodigoqr');

$ADMIN->add('frontpage', $externalpage);

if ($hassiteconfig) {

    $settings = new admin_settingpage('local_codigoqr', 'Configuración local codigoqr');
 
	// Create 
    $ADMIN->add('localplugins', $settings);
 
	// Add a setting field to the settings for this page
    $settings->add(new admin_setting_configtext(
 
		// This is the reference you will use to your configuration
        'local_codigoqr/config',
 
		// This is the friendly title for the config, which will be displayed
        'nombre',
 
		// This is helper text for this config field
        'ayuda nombre',
		// This is the default value
        'default value nombre',
 
		// This is the type of Parameter this config is
        PARAM_TEXT

    ));

}
